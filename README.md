About
=====
*Tale of Kingdoms: A new Conquest* is a revival of the Tale of Kingdoms mod. Currently we are in pre-alpha. Content seen is based on original versions.

Links
=====
Website: https://www.convallyria.com

Discord: https://discord.gg/fh62mxU

FAQ
=====
Q: Who is working on it?
A: I am the only developer working on this. As such, this is going to take quite a while! Contributions are extremely welcome.

Q: How can I help?
A: To see what needs help with, check out the open [issues](https://gitlab.com/SamB440/tale-of-kingdoms/-/issues). Clone the repository and make a [merge request](https://gitlab.com/SamB440/tale-of-kingdoms/-/merge_requests).

Q: How do I install it?
A: You cannot currently install it. The mod is in pre-alpha. If you want, you can download the source code and compile it yourself.

Q: Will you be adding more to the mod?
A: Yes, we will be adding more to the mod. This page will only contain the core mod, allowing other mods to interact and add to it. Some addons will be made to the base mod that you will be able to download for free.

Q: Where can I donate?
A: Currently you cannot donate simply because I have not set anything up. However, in the future you will be able to.

Q: What versions will it be for?
A: As I am the only one working on it, only 1.15+ will be supported.

Minecraft Server
=======
IslandEarth - https://www.islandearth.net
